<?php

namespace Drupal\etherapi\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\FormBase;

/**
 * Class SettingsForm.
 */
class SettingsForm extends FormBase {

  /**
   * EtherAPI service.
   *
   * @var \Drupal\etherapi\EtherAPI
   */
  protected $etherapi;

  /**
   * Ajax info.
   *
   * @var array
   */
  protected $ajax;

  /**
   * Config info.
   *
   * @var object
   */
  protected $config;

  /**
   * Constructs.
   */
  public function __construct() {
    $this->etherapi = \Drupal::service('EtherAPI');
    $this->ajax = [
      'wrapper' => 'etherapi_settings_form_ajax_wrap',
      'callback' => '::ajaxSubmit'
    ];
    $this->config = \Drupal::config('etherapi.settings');
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'etherapi_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form += [
      '#prefix' => '<div id="'.$this->ajax['wrapper'].'">',
      '#suffix' => '</div>',
      'status_messages' => [
        '#type' => 'status_messages'
      ]
    ];
    $form['config'] = [
      '#tree' => TRUE,
      'currency' => [
        '#type' => 'select',
        '#options' => [
          'ETH'  => 'ETH',
          'USDT' => 'USDT',
        ],
        '#title' => $this->etherapi->t('Default currency'),
        '#default_value' => $this->config->get('config.currency'),
        '#required' => TRUE
      ],
      'keys' => [
        '#type' => 'table',
        '#caption' => $this->etherapi->t('API access info'),
        '#header' => [
          $this->etherapi->t('Currency'),
          $this->etherapi->t('API access key'),
          $this->etherapi->t('Address for collecting received coins'),
        ],
      ],
      'REMOTE_ADDR' => [
        '#type' => 'textarea',
        '#title' => '$_SERVER[\'REMOTE_ADDR\']',
        '#default_value' => $this->config->get('config.REMOTE_ADDR'),
      ]
    ];

    foreach($form['config']['currency']['#options'] as $currency){
      $form['config']['keys'][$currency] = [
        'currency' => [
          '#markup' => $currency,
        ],
        'key' => [
          '#type' => 'textfield',
          '#title' => $this->etherapi->t('API access key'),
          '#title_display' => 'invisible',
          '#default_value' => $this->config->get('config.keys.'.$currency.'.key')
        ],
        'address' => [
          '#type' => 'textfield',
          '#title' => $this->etherapi->t('Address for collecting received coins'),
          '#title_display' => 'invisible',
          '#default_value' => $this->config->get('config.keys.'.$currency.'.address')
        ],
      ];
    }
    $form['actions'] = [
      '#type' => 'actions',
      'submit' => [
        '#type' => 'submit',
        '#name' => 'save',
        '#value' => t('Save configuration'),
        '#attributes' => [
          'class' => ['button--primary']
        ],
        '#ajax' => $this->ajax
      ]
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function ajaxSubmit(array &$form, FormStateInterface $form_state) {
    if ($form_state->isSubmitted() && !$form_state->getErrors()) {
      $config = \Drupal::configFactory()->getEditable('etherapi.settings');
      $configSave = $form_state->getValue('config');
      $config->set('config', $configSave);
      $config->save();
      \Drupal::messenger()->addMessage(t('The configuration options have been saved.'));
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {}

}
