<?php

namespace Drupal\etherapi\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Url;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Markup;

/**
 * Class SettingsForm.
 */
class PaymentForm extends FormBase {

  const API_URL = 'https://etherapi.net/api/v2/';

  /**
   * EtherAPI service.
   *
   * @var \Drupal\etherapi\EtherAPI
   */
  protected $etherapi;

  /**
   * Config info.
   *
   * @var object
   */
  protected $config;

  /**
   * Constructs.
   */
  public function __construct() {
    $this->etherapi = \Drupal::service('EtherAPI');
    $this->config = \Drupal::config('etherapi.settings');
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'etherapi_payment_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $payment = NULL) {
    // Payment alter
    $this->basketPaymentFormAlter($form, $form_state, $payment);
    // ---
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {}

  /**
   * {@inheritdoc}
   */
  public function basketPaymentFormAlter(&$form, &$form_state, $payment) {
    // ---
    if(!empty($form['timer']))    unset($form['timer']);
    if(!empty($form['actions']))  unset($form['actions']);
    // ---
    $form['#id'] = 'payment_form';
    $form['info'] = [
      '#theme' => 'etherapi_pay',
      '#info' => [
        'label' => $this->etherapi->t('Sum'),
        'payment' => $payment
      ],
      '#attached' => [
        'library' => [
          'etherapi/css'
        ]
      ]
    ];
    // ---
    $langcode = \Drupal::languageManager()->getCurrentLanguage()->getId();
    $config = $this->config->get('config');

    $form['#params'] = [
      'amount' => $payment->amount,
      'currency' => $payment->currency,
    ];
    // Alter
    \Drupal::moduleHandler()->alter('etherapi_payment_params', $form['#params'], $payment, $config);
    // Метод give
    $apiKey = (string) @$config['keys'][$payment->currency]['key'];
    $data = [
      'key' => trim($apiKey),
      'tag' => $payment->id,
      'uniqID' => $payment->id,
      'statusURL' => Url::fromRoute('etherapi.pages', ['page_type' => 'status'], ['absolute' => TRUE])->toString()
    ];
    if($payment->currency != 'ETH') $data['token'] = $payment->currency;
    if(!empty($config['keys'][$payment->currency]['address'])) {
      $data['address'] = trim($config['keys'][$payment->currency]['address']);
    }
    try {
      $response = \Drupal::httpClient()->get($this::API_URL.'.give?'.http_build_query($data));
      $resData = $response->getBody()->getContents();
      if(!empty($resData)) {
        $resData = @json_decode($resData, TRUE);
        if(!empty($resData['error'])) {
          \Drupal::messenger()->addMessage($resData['error'], 'error');
        }
        elseif(!empty($resData['result'])) {
          $form['#params']['address'] = is_array($resData['result']) ? $resData['result']['address'] : $resData['result'];
        }
      }
    } catch ( \Exception $e) {}

    $form['info']['#info']['currency'] = $form['#params']['currency'];
    $form['info']['#info']['sum'] = $form['#params']['amount'];
    $form['info']['#info']['pay'] = $this->etherapi->t('Payment should be sent to this address @address@', [
      '@address@' => Markup::create('<strong>'. ($form['#params']['address'] ?? '- - -') .'</strong>')
    ]);
    // ---
    $form['mess'] = [
      '#type' => 'status_messages'
    ];
  }

}
