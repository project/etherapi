<?php

namespace Drupal\etherapi\Controller;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Pages {

	/**
   * EtherAPI service.
   *
   * @var \Drupal\etherapi\EtherAPI
   */
  protected $etherapi;

  /**
   * Basket service.
   *
   * @var \Drupal\basket\Basket
   */
  protected $basket;

  /**
   * Page Query Params.
   *
   * @var array
   */
  protected $query;

  /**
   * Constructs.
   */
  public function __construct() {
    $this->etherapi = \Drupal::service('EtherAPI');
    $this->basket = \Drupal::hasService('Basket') ? \Drupal::service('Basket') : NULL;
    $this->query = \Drupal::request()->query->all();
  }

	public function title(array $_title_arguments = array(), $_title = ''){
    return $this->etherapi->t($_title);
  }

  public function pages($page_type) {
    $element = [];
  	switch($page_type) {
  		case'pay':
        $is_404 = TRUE;
        $payment = $this->etherapi->load([
          'id' => $this->query['pay_id'] ?? '-'
        ]);
        if(!empty($payment) && $payment->status == 'new') {
          $is_404 = FALSE;
          $element['form'] = \Drupal::formBuilder()->getForm('\Drupal\etherapi\Form\PaymentForm', $payment);
        }
        if($is_404){
          throw new NotFoundHttpException();
        }
      break;
      case'status':
        $config = \Drupal::config('etherapi.settings')->get('config');
        if(!empty($config['REMOTE_ADDR'])) {
          $REMOTE_ADDR = explode(PHP_EOL, $config['REMOTE_ADDR']);
          if (!in_array($_SERVER['REMOTE_ADDR'], $REMOTE_ADDR)) {
            throw new NotFoundHttpException();
          }
        }
        if (!$_POST) {
          $_POST = @json_decode(file_get_contents('php://input'), true);
        }
        if (empty($_POST['etherapi.net'])) {
          throw new NotFoundHttpException();
        }
				
        $postToken = $_POST['token'] ?? 'ETH';
        $payment = $this->etherapi->load([
          'id' => $_POST['tag'] ?? '-'
        ]);
        if(!empty($payment->currency) && $payment->currency == $postToken) {
          $apiKey = (string) @$config['keys'][$payment->currency]['key'];

          $signData = [
            $_POST['type'] ?? '',
            $_POST['date'] ?? '',
            $_POST['from'] ?? '',
            $_POST['to'] ?? '',
          ];
          if($postToken != 'ETH'){
            $signData[] = $postToken;
          }
          $signData = array_merge($signData, [
            $_POST['amount'] ?? '',
            $_POST['txid'] ?? '',
            $_POST['confirmations'] ?? '',
            $_POST['tag'] ?? '',
            trim($apiKey)
          ]);
          $sign = sha1(implode(':', $signData));

          if ($sign === $_POST['sign']) {
            $data = $_POST;
            $prePay = @unserialize($payment->data);
            $data['pre_pay'] = !empty($prePay['pre_pay']) ? $prePay['pre_pay'] : $prePay;
            $data['pays'] = !empty($prePay['pays']) ? $prePay['pays'] : [];
            $data['pays'][] = $_POST;
            // ---
            $payment->paytime = time();
            $payment->data = serialize($data);
            $payment->status = 'pay';
            // ---
            $this->etherapi->update($payment);
            // ---
            if(!empty($payment->nid) && !empty($this->basket)) {
              if(method_exists($this->basket, 'paymentFinish')) {
                $this->basket->paymentFinish($payment->nid);
              }
            }
            // Alter
            \Drupal::moduleHandler()->alter('etherapi_api', $payment, $_POST);
            // Trigger change payment status by order
            if(!empty($payment->nid) && !empty($this->basket)){
              $order = $this->basket->Orders(NULL, $payment->nid)->load();
              if(!empty($order) && \Drupal::moduleHandler()->moduleExists('basket_noty')){
                \Drupal::service('BasketNoty')->trigger('change_etherapi_status', [
                  'order' => $order
                ]);
              }
            }
            exit('OK');
          }
        }
        exit('Sign wrong');
      break;
  	}
  	return $element;
  }
}