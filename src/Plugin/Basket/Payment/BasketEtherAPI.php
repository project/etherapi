<?php

namespace Drupal\etherapi\Plugin\Basket\Payment;

use Drupal\basket\Plugins\Payment\BasketPaymentInterface;
use Drupal\basket\Plugins\Payment\Annotation\BasketPayment;
use Drupal\etherapi\Form\PaymentForm;
use Drupal\etherapi\Controller\Pages;

/**
 * @BasketPayment(
 *   id = "etherapi",
 *   name = "EtherAPI",
 * )
 */
class BasketEtherAPI implements BasketPaymentInterface {

	/**
   * Drupal\basket\Basket definition.
   *
   * @var \Drupal\basket\Basket
   */
  protected $basket;

  /**
   * Drupal\etherapi\EtherAPI definition.
   *
   * @var \Drupal\etherapi\EtherAPI
   */
  protected $etherapi;

  /**
   * Constructs.
   */
  public function __construct() {
    $this->basket = \Drupal::service('Basket');
    $this->etherapi = \Drupal::service('EtherAPI');
  }

  /**
	 * Alter for making adjustments when creating a payment item
	 */
	public function settingsFormAlter(&$form, $form_state) {
		$tid = $form['tid']['#value'];
		$form['settings'] = [
			'#type' => 'details',
			'#title' => t('Settings'),
			'#open'	=> TRUE,
			'#parents' => ['basket_payment_settings'],
			'#tree' => TRUE,
			'status' => [
				'#type' => 'select',
				'#title' => $this->etherapi->t('Change order status after payment to'),
				'#options' => $this->basket->Term()->getOptions('fin_status'),
				'#empty_option'	=> $this->etherapi->t('Do not change status'),
				'#default_value' => $this->basket->getSettings('payment_settings', $tid.'.status')
			]
		];
		$form['#submit'][] = [$this, 'formSubmit'];
	}

	/**
	 * Alter for making adjustments when creating a payment item (submit)
	 */
	public function formSubmit($form, $form_state){
		$tid = $form_state->getValue('tid');
		if(!empty($tid)){
			$this->basket->setSettings(
				'payment_settings', 
        $tid, 
        $form_state->getValue('basket_payment_settings')
			);
		}
	}

	/**
	 * Interpretation of the list of settings for pages with payment types
   */
	public function getSettingsInfoList($tid) {
		$items = [];
		if(!empty($settings = $this->basket->getSettings('payment_settings', $tid))){
			$value = $this->etherapi->t('Do not change status');
			if(!empty($settings['status']) && !empty($term = $this->basket->Term()->load($settings['status']))){
				$value = $this->basket->Translate()->t($term->name);
			}
			$items[] = [
				'#type' => 'inline_template',
				'#template' => '<b>{{ label }}: </b> {{ value }}',
				'#context' => [
					'label'	=> $this->etherapi->t('Change order status after payment to'),
					'value'	=> $value
				]
			];
		}
		return $items;
	}

	/**
	 * Creation of payment
	 */
	public function createPayment($entity, $order) {
		if(!empty($order->pay_price)){
			$currency = $this->basket->Currency()->load($order->pay_currency);
			$payment = $this->etherapi->load([
				'nid' => $entity->id(),
				'create_new' => TRUE,
				'amount' => $order->pay_price
			]);
			if(!empty($payment)){
				return [
					'payID' => $payment->id,
					'redirectUrl'	=> NULL
				];
			}
		}
		return [
			'payID' => NULL,
			'redirectUrl'	=> NULL
		];
	}

	/**
	 * Alter page redirects for payment
	 */
	public function loadPayment($id) {
		$payment = $this->etherapi->load(['id' => $id]);
		if(!empty($payment)){
			return [
				'payment' => $payment,
				'isPay' => $payment->status != 'new' ? TRUE : FALSE
			];
		}
	}

	/**
	 * Alter page redirects for payment
	 */
	public function paymentFormAlter(&$form, $form_state, $payment) {
		(new PaymentForm)->basketPaymentFormAlter($form, $form_state, $payment);
	}

	/**
	 * Alter processing pages of interaction between the payment system and the site
	 */
	public function basketPaymentPages($pageType) {
		return (new Pages)->pages('status');
	}

	/**
	 * Order update upon successful ordering by payment item settings
	 */
	public function updateOrderBySettings($pid, $orderClass) {
		$order = $orderClass->load();
		if(!empty($order) && !empty($settings = $this->basket->getSettings('payment_settings', $pid))){
			if(!empty($settings['status'])){
				$order->fin_status = $settings['status'];
				$orderClass->replaceOrder($order);
				$orderClass->save();
			}
		}
	}
}
